package oidc

import (
	"fmt"
	"net/http"
)

func OtherHandler(w http.ResponseWriter, r *http.Request) {
	fmt.Fprint(w, "Hello, "+r.URL.Path[1:])
}
